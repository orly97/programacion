Algoritmo EnteroCondicion
	Leer num
	esDecimal <- num<>Trunc(num)
	Si esDecimal Entonces
		Escribir 'El numero es decimal'
	SiNo
		Escribir 'El numero es entero'
	FinSi
FinAlgoritmo
