Algoritmo Ejemplo_Serie_Farey
	
	//1/1, 1/2, 1/3, 2/1, 2/2, 2/3, 3/1, 3/2, 3/3
	Escribir "Ingresa el tama�o de la serie de Farey"
	leer tama�o
	
	numerador = 1
	denominador = 1
	
	mostrar numerador "/" denominador
	
	Repetir		
		si denominador = tama�o 
			numerador = numerador + 1
			denominador = 1
			mostrar numerador "/" denominador
		SiNo
			denominador = denominador + 1
			mostrar numerador "/" denominador
		fin si
	Hasta Que numerador = tama�o y denominador = tama�o
	
FinAlgoritmo
