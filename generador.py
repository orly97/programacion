#Programa que genera archivos de input para pruebas automatizadas de algoritmos
#Autor: Sergio Castro

#Librerias
import random

#Leer numero de archivos
archives = int(input("Ingrese el numero de archivos a generar: "))

#Leer numero de lineas
numberLines = int(input("Ingrese el numero de lineas: "))

#Leer rango min y max por linea
minList = []
maxList = []
for i in range(numberLines):
    minList.append(int(input("Ingrese el rango mínimo para la linea " + str(i+1) + ": ")))
    maxList.append(int(input("Ingrese el rango máximo para la linea " + str(i+1) + ": ")))


#Generar archivos
for i in range(archives):
    f = open("input-"+str(i+1)+".txt", "w")
    for j in range(numberLines):
        f.write(str(random.randint(minList[j], maxList[j])) + "\n")
    f.close()
