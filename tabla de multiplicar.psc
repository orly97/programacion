Algoritmo tabla_de_multiplicar
	contador<-0
	Escribir 'Este programa muestra la tabla de multiplicar de un numero.'
	Repetir
		Escribir 'Escriba el numero de la tabla de multiplicar que desea';
		Leer num;
		Si num>=0 y num<=100 Entonces
			Escribir 'Tabla de multiplicar del: ', num;
		SiNo
			Escribir 'Error, vuelva a intentar';
		Fin Si
	Hasta Que num>=0 y num<=100;
	Repetir
		contador<-contador+1;
		Escribir num,' x ',contador,' = ',num*contador;
	Hasta Que contador=12
FinAlgoritmo
