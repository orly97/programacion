Proceso ParesVSImpares
	//Realizar un algoritmo que pida 5 n�meros, indique cu�ntos n�meros pares y cu�ntos n�meros impares ingresaron, y luego muestre el resultado de restar la suma de los pares menos la suma de los impares.
	
	/// Input
	Escribir "Ingresa un n�mero"
	Leer num1

	Escribir "Ingresa un segundo n�mero"
	Leer num2

	Escribir "Ingresa un tercer n�mero"
	Leer num3

	Escribir "Ingresa un cuarto n�mero"
	Leer num4

	Escribir "Ingresa un quinto n�mero"
	Leer num5

		
	/// Tu soluci�n
	
	numeroPares = 0
	sumaPares = 0

	numeroImpares = 0
	sumaImpares = 0


	si (num1 % 2 == 0)
		numeroPares = numeroPares + 1
		sumaPares = sumaPares + num1
	Sino
		numeroImpares = numeroImpares + 1
		sumaImpares = sumaImpares + num1
	FinSi

	si (num2 % 2 == 0)
		numeroPares = numeroPares + 1
		sumaPares = sumaPares + num2
	Sino
		numeroImpares = numeroImpares + 1
		sumaImpares = sumaImpares + num2
	FinSi

	si (num3 % 2 == 0)
		numeroPares = numeroPares + 1
		sumaPares = sumaPares + num3
	Sino
		numeroImpares = numeroImpares + 1
		sumaImpares = sumaImpares + num3
	FinSi

	si (num4 % 2 == 0)
		numeroPares = numeroPares + 1
		sumaPares = sumaPares + num4
	Sino
		numeroImpares = numeroImpares + 1
		sumaImpares = sumaImpares + num4
	FinSi

	si (num5 % 2 == 0)
		numeroPares = numeroPares + 1
		sumaPares = sumaPares + num5
	Sino
		numeroImpares = numeroImpares + 1
		sumaImpares = sumaImpares + num5
	FinSi

	Escribir "Hay " numeroPares " n�meros pares y " numeroImpares " n�meros impares"
	Escribir "La suma de los pares es " sumaPares " y la suma de los impares es " sumaImpares
	Escribir "La diferencia es " (sumaPares - sumaImpares)

FinProceso
