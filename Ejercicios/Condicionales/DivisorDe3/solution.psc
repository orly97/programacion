Proceso DivisorDe3
	
	/// Input
	Escribir "Ingresa un n�mero";
	Leer num1

	Escribir "Ingresa un segundo n�mero";
	Leer num2

	Escribir "Ingresa un tercer n�mero";
	Leer num3

	/// Tu soluci�n
	si num2 % num1 == 0 y num3 % num1 == 0 Entonces
		Escribir "El n�mero " num1 " es divisor com�n de los otros 2 n�meros" 
	sino
		si num1 % num2 == 0 y num3 % num2 == 0 Entonces
			Escribir "El n�mero " num2 " es divisor com�n de los otros 2 n�meros"
		sino
			si num1 % num3 == 0 y num2 % num3 == 0 Entonces
				Escribir "El n�mero " num3 " es divisor com�n de los otros 2 n�meros"
			sino
				Escribir "Ninguno de los n�meros es divisor del otro"
			FinSi
		FinSi
	FinSi

FinProceso
