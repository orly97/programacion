Proceso DivisorExacto
	
	/// Input
	Escribir "Ingresa un n�mero";
	Leer num1

	Escribir "Ingresa un segundo n�mero";
	Leer num2

		
	/// Tu soluci�n
	si num2 % num1 == 0 Entonces
		Escribir "El n�mero " num1 " es divisor exacto del n�mero " num2
	sino
		si num1 % num2 == 0 Entonces
			Escribir "El n�mero " num2 " es divisor exacto del n�mero " num1
		sino
			Escribir "Ninguno de los n�meros es divisor del otro"
		FinSi
	FinSi
FinProceso
