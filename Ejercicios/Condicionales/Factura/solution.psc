Proceso Factura	

	/// Input
	Escribir "Ingresa la cantidad a comprar del producto";
	Leer cantidad

	Escribir "Ingresa el precio del producto";
	Leer precioProducto

		
	/// Tu soluci�n
	precioBase = precioProducto * cantidad;
	costoIva = precioBase * 0.16;
	precioBruto = precioBase + costoIva;
	descuento = 0;

	precioAPagar = precioBruto;

	Si precioBruto > 500000
		descuento = precioBase * 0.15;
		precioAPagar = precioBruto - descuento;
	FinSi

	/// Output
	Escribir ""
	Escribir "Precio del producto: " precioProducto;
	Escribir "Cantidad: " cantidad;
	Escribir "Total base: " precioBase;
	Escribir "IVA (16%): " costoIva;
	Escribir "Total bruto: " precioBruto;
	Escribir "Descuento: " descuento;
	Escribir ""
	Escribir "Total a pagar: " precioAPagar;
FinProceso
