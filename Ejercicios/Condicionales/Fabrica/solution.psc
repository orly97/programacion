Proceso Fabrica	

	//Una fábrica de tornillos hace controles de calidad tomando una muestra del 10% de los tornillos por cada lote de tornillos que sacan, dependiendo de la calidad de los tornillos se harán descuentos para venderlos, si el porcentaje de tornillos malos es menor al 5% se venderá al 100% del precio del lote, si el porcentaje está entre el 5% y 10% se hará un descuento del 10% al precio del lote, si el porcentaje de tornillos dañados es del 10 al 15% al descuento original se le restará el 5% + 10000 pesos del precio del lote, si el porcentaje es del 15 al 30%, se hará un descuento del 20% y se hará un llamado de atención a las personas que hicieron el lote, si el porcentaje de tornillos dañados está entre el 30 y 50% se aplicará el descuento del 10% a 15% + el porcentaje de tornillos dañados dividido en 2, y se diezmara el grupo que hizo el lote, si el porcentaje supera el 50% el lote no se venderá y se desintegrará el grupo que realizó el lote, con posible despido del total de sus integrantes (suponga que es una empresa romana de ahí los castigos).
	//Realizar un algoritmo que reciba el precio del lote de los tornillos, el número de tornillos por lote (el 10% a evaluar), y el número de tornillos dañados (del 10% evaluado) y muestre el precio del lote de tornillos.

	/// Input
	Escribir "Ingrese el precio del lote de tornillos"
	Leer precioLote

	Escribir "Ingrese el número de tornillos por lote"
	Leer tornillosPorLote

	Escribir "Ingrese el número de tornillos dañados"
	Leer tornillosDañados
		
	/// Tu solución

	//Calculo del porcentaje de tornillos dañados
	porcentajeTornillosDañados = tornillosDañados * 100 / tornillosPorLote

	descuento = 0
	
	Si porcentajeTornillosDañados >= 5 y porcentajeTornillosDañados < 10
		//10% de descuento
		descuento = precioLote * 0.1	
		Sino Si porcentajeTornillosDañados >= 10 y porcentajeTornillosDañados < 15
			//15% de descuento + 10000 pesos 
			descuento = precioLote * 0.15 + 10000
			Sino Si porcentajeTornillosDañados >= 15 y porcentajeTornillosDañados < 30
				//20% de descuento
				descuento = precioLote * 0.2
				Escribir "Llamada de atención, el lote tiene muy mala calidad"
				Sino Si porcentajeTornillosDañados >= 30 y porcentajeTornillosDañados < 50
					//15% de descuento + 10000 pesos + el porcentaje de tornillos dañados dividido en 2
					descuento = precioLote * (0.15 + (porcentajeTornillosDañados / 200)) + 10
					Escribir "El lote es muy malo y se despedirá al 10% del grupo"
					Sino Si porcentajeTornillosDañados >= 50
						//No se vende el lote
						descuento = precioLote
						Escribir "El lote es inaceptable y se desintegra el grupo"
					FinSi
				FinSi
			FinSi
		FinSi
	FinSi

	//Calculo del precio del lote
	precioAPagar = precioLote - descuento
	

	Escribir "A continuación se mostrará el precio del lote de tornillos"
	Escribir precioAPagar


FinProceso
