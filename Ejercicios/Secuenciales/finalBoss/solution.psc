Proceso NotaEvaluaciónSolución
	
	/// Input
	Escribir "Ingresa las notas de los 3 promedios anteriores";
	Leer promedio1, promedio2, promedio3;
	
	/// Tu algoritmo aquí
	
	promedioFaltante = (promedio1*0.10 + promedio2*0.20 + promedio3*0.15);
	notaNecesariaParaPasar = (75 - promedioFaltante) / 0.55;
	
	/// Output
	Escribir "Necesitas sacar " notaNecesariaParaPasar " en la evaluación para pasar la materia"; 
FinProceso
